﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;
using System.Linq;
using Newtonsoft.Json;
using System.Net;
using System.Net.Sockets;
using System.Text;

public class GridAgent : Agent
{
    [Header("Specific to GridWorld")]
    public GridAcademy academy;

    [HideInInspector]
    public int gridSize;

    GameObject trueAgent;

    /**
	* Use this method to initialize your agent.
	* This method is called then the agent is created.
    * This method called only once when the agent is enabled.
	*/
    public override void InitializeAgent()
    {
        trueAgent = gameObject;
        gridSize = (int)academy.resetParameters["gridSize"];
    }

    /**
    * Must return a list of floats corresponding to the state the agent is in.
    * If the state space type is discrete,
    * return a list of length 1 containing the float equivalent of your state.
    * This method called at every step and collects the state of the agent.
    */
    public override List<float> CollectState()
    {
        int closestGoalDistance = 2 * (int)academy.resetParameters["gridSize"];
        GameObject currentClosestGoal = academy.actorObjs[0];

        int closestPitDistance = 2 * (int)academy.resetParameters["gridSize"];
        GameObject currentClosestPit = academy.actorObjs[0];
        GameObject agent = academy.actorObjs[0];

        // Compared the tag of each gameObject which is defined in Inspector
        List<float> state = new List<float>();
        foreach (GameObject actor in academy.actorObjs)
        {
            if (actor.tag == "agent")
            {
                agent = actor;
                state.Add(actor.transform.position.x / (gridSize + 1)); // agent moves Up, Down, Right and Left
                state.Add(actor.transform.position.z / (gridSize + 1));
                continue;
            }
        }

        foreach (GameObject actor in academy.actorObjs)
        {
            if (actor.tag == "goal")
            {
                int distance = (int)Mathf.Abs(agent.transform.position.x - actor.transform.position.x) + (int)Mathf.Abs(agent.transform.position.z - actor.transform.position.z);
                if (closestGoalDistance > distance)
                {
                    closestGoalDistance = distance;
                    currentClosestGoal = actor;
                }
            }
            if (actor.tag == "pit")
            {
                int distance = (int)Mathf.Abs(agent.transform.position.x - actor.transform.position.x) + (int)Mathf.Abs(agent.transform.position.z - actor.transform.position.z);
                if (closestPitDistance > distance)
                {
                    closestPitDistance = distance;
                    currentClosestPit = actor;
                }
            }
        }

        state.Add(currentClosestGoal.transform.position.x / (gridSize + 1));
        state.Add(currentClosestGoal.transform.position.z / (gridSize + 1));
        state.Add(currentClosestPit.transform.position.x / (gridSize + 1));
        state.Add(currentClosestPit.transform.position.z / (gridSize + 1));

        return state;
    }

    /**
    * This function will be called every frame, you must define what
    * your agent will do given the input actions.
    * You must also specify the rewards and whether or not the agent is done.
    * To do so, modify the public fields of the agent 'reward' and 'done'.
    */
    public override void AgentStep(float[] act)
    {

        reward = -1f;
        int action = Mathf.FloorToInt(act[0]);

        // 0 - Forward, 1 - Backward, 2 - Left, 3 - Right
        if (action == 3)
        {
            Collider[] blockTest = Physics.OverlapBox(new Vector3(trueAgent.transform.position.x + 1, 0, trueAgent.transform.position.z), new Vector3(0.3f, 0.3f, 0.3f));
            if (blockTest.Where(col => col.gameObject.tag == "wall").ToArray().Length == 0)
            {
                trueAgent.transform.position = new Vector3(trueAgent.transform.position.x + 1, 0, trueAgent.transform.position.z);
            }
        }

        if (action == 2)
        {
            Collider[] blockTest = Physics.OverlapBox(new Vector3(trueAgent.transform.position.x - 1, 0, trueAgent.transform.position.z), new Vector3(0.3f, 0.3f, 0.3f));
            if (blockTest.Where(col => col.gameObject.tag == "wall").ToArray().Length == 0)
            {
                trueAgent.transform.position = new Vector3(trueAgent.transform.position.x - 1, 0, trueAgent.transform.position.z);
            }
        }

        if (action == 0)
        {
            //Debug.Log("Action 0-Forward");
            Collider[] blockTest = Physics.OverlapBox(new Vector3(trueAgent.transform.position.x, 0, trueAgent.transform.position.z + 1), new Vector3(0.5f, 0.25f, 4f));
            if (blockTest.Where(col => col.gameObject.tag == "wall").ToArray().Length == 0)
            {
                Debug.Log("You hit the wall.");
                trueAgent.transform.position = new Vector3(trueAgent.transform.position.x, 0, trueAgent.transform.position.z + 1);
            }
        }

        if (action == 1)
        {
            Collider[] blockTest = Physics.OverlapBox(new Vector3(trueAgent.transform.position.x, 0, trueAgent.transform.position.z - 1), new Vector3(0.3f, 0.3f, 0.3f));
            if (blockTest.Where(col => col.gameObject.tag == "wall").ToArray().Length == 0)
            {
                trueAgent.transform.position = new Vector3(trueAgent.transform.position.x, 0, trueAgent.transform.position.z - 1);
            }
        }

        Collider[] hitObjects = Physics.OverlapBox(trueAgent.transform.position, new Vector3(0.3f, 0.3f, 0.3f));
        if (hitObjects.Where(col => col.gameObject.tag == "goal").ToArray().Length == 1)
        {
            //Debug.Log("Hit GOAL!");
            reward = 1f;
            done = true;
        }
        if (hitObjects.Where(col => col.gameObject.tag == "pit").ToArray().Length == 1)
        {
            //Debug.Log("Hit PIT!");
            reward = -1f;
            done = true;
        }

        //if (trainMode == "train") { (!?????)
        if (true)
        {
            academy.visualAgent.transform.position = trueAgent.transform.position;
            academy.visualAgent.transform.rotation = trueAgent.transform.rotation;
        }
    }

    // to be implemented by the developer
    public override void AgentReset()
    {
        //academy.AcademyReset();
    }
}
